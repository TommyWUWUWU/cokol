/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

// ElementUI
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component(
    "checkbox-component",
    require("./components/CheckboxComponent.vue")
);
// 輸入文字
Vue.component("input_cokol", require("./components/InputTextCokol.vue"));
// 日期
Vue.component("datepicker_cokol", require("./components/DatePickerCokol.vue"));

// 日期時間範圍
Vue.component(
    "daterangepicker_cokol",
    require("./components/DateRangePickerCokol.vue")
);
Vue.component("btn_cokol", require("./components/button.vue"));

Vue.component("checkbox_cokol", require("./components/CheckboxCokol.vue"));
Vue.component("radio_cokol", require("./components/radioCokol.vue"));
Vue.component("select_cokol", require("./components/selectCokol.vue"));

new Vue({
    el: ".wrapper"
});
