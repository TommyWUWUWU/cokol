<!-- Sidebar  -->
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            @foreach(Auth::user()->menus() as $menu)
                <li class='@if( starts_with(Request::path(), $menu->url) ) active @endif @if( count($menu->children) ) treeview @endif' >
                    <a href='{{ url($menu->url) }}'>
                        <i class='{{ $menu->icon }}'></i> 
                        <span>{{ $menu->name }}</span>
                        <span class='pull-right-container'>
                            @if( count($menu->children) )
                                <i class='fa fa-angle-left pull-right'></i>
                            @endif
                        </span>
                    </a>
                @if( count($menu->children) )
                    <ul class='treeview-menu'>
                    @foreach($menu->children as $child)
                        <li class='@if( starts_with(Request::path(), $child->url) ) active @endif'>
                            <a href='{{ url($child->url) }}'><i class='{{ $child->icon }}'></i> {{ $child->name }}</a>
                        </li>
                    @endforeach
                    </ul>
                @endif
                </li>

            @endforeach
        </ul>
    </section>
</aside>
