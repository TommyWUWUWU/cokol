<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- for responsive -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- bootstrap -->
    <link href="{{asset("adminlte/bootstrap.css")}}" rel="stylesheet" type="text/css" />
    <!-- font awesome -->
        <link href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" rel="stylesheet" type="text/css">
    <!-- ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- adminLTE style -->
    <link href="{{asset("adminlte/adminlte.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("adminlte/skin-blue.css")}}" rel="stylesheet" type="text/css" />
    <!-- JS -->

    <!-- jquery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <!-- bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- adminLTE -->
    <script src="{{ asset ("/adminlte/adminlte.js") }}" type="text/javascript"></script>

    <!-- Select2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <!-- Sweetalert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.5/dist/sweetalert2.all.min.js"></script>
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <!-- ヘッダー -->
        @include('layouts.header')

        <!-- サイドバー -->
        @include('layouts.sidebar')

        <!-- content -->
        <div class="content-wrapper">
                       
            <!-- コンテンツ -->
            @yield('content')

        </div><!-- end content -->

        <!-- フッター -->
        @include('layouts.footer')


    </div><!-- end wrapper -->
</body>
</html>