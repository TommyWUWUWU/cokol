@extends('layouts.admin')

@section('content')
<!-- header -->
<section class="content-header">
	<h1>{{ __( 'client.header.title' ) }}</h1>
</section>

<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">{{ __( 'client.content.title' ) }}</h3>

			<div class="box-tools">
			<a href="{{ url('client/create') }}" class="btn btn-primary btn-sm ad-click-event"><i class="fas fa-plus"></i></a>
			</div>
		</div>

		<table id="example" class="table table-hover table-bordered" style="width:100%">
			<thead>
				<tr>
					<td>{{ __( 'client.table.id' ) }}</td>
					<td>{{ __( 'client.table.name' ) }}</td>
					<td></td>
				</tr>
			</thead>
			<tbody>	
				@foreach($clients as $client)
				<tr>
					<td>{{ $client->client_id }}</td>
					<td>{{ $client->client_name }}</td>
					<td>
						<a href="{{ url('client', $client->client_id) }}" class="btn btn-primary btn-sm ad-click-event"><i class="fas fa-pen"></i></a>
						
    					<form client="form" method="POST" action="{{ url('client', $client->client_id ?? '' ) }}">
							@method('DELETE')
							@csrf
							<button type="submit" class="btn btn-danger btn-sm ad-click-event"><i class="fas fa-times"></i></button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

	</div>
</section>
@endsection