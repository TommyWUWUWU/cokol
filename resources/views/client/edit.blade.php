@extends('layouts.admin')


@section('content')
<!-- header -->
<section class="content-header">
	<h1>{{ __( 'client.header.title' ) }}</h1>
</section>


<!-- メインコンテンツ -->
<section class="content">

<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title">{{ __( 'client.content.title' ) }}</h3>
	</div>

    <form client="form" method="POST" action="{{ url('client') }}">
        @csrf
        <div class="box-body">
            
			<input type="hidden" class="form-control" name="client_id" placeholder="" value="{{ $client->client_id ?? '' }}">
			
            @component('component.input_text', [
				'key' => 'client_name', 
				'value' => $client->client_name, 
				'label' => __('client.form.name') ])
			@endcomponent

        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        <button type="submit" class="btn btn-primary"><i class="fas fa-check"></i> Submit</button>
        </div>
    </form>
</div>

</section>
@endsection