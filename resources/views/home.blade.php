@extends('layouts.admin')

@section('content')

<!-- コンテンツヘッダ -->
<section class="content-header">
    <h1>Home</h1>
</section>

<!-- メインコンテンツ -->
<section class="content">

    <!-- コンテンツ1 -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">XXX</h3>
        </div>
        <div class="box-body">
            <p>body</p>
        </div>
        <div id="tNum">
            <p>@{{msg}}</p>
        </div>
    </div>

</section>
@endsection
