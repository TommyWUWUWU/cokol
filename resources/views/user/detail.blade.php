@extends('layouts.admin')
<style>
    .col-md-2 img {
        width: 100%;
    }
    ul li {
        list-style: none;
        padding: 10px 0;
    }
</style>
@section('content')
<!-- header -->

<!-- コンテンツヘッダ -->
<!-- <div id="app"> -->
<section class="content-header">
    <h1>{{$users->user_kol_name}}</h1>
</section>

<!-- メインコンテンツ -->
<section class="content">
<div class="box">
    <div class="box-header with-border">
        <!-- <h3 class="box-title">{{$users->id}}</h3> -->
    </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-2">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA2FBMVEX/zGb/////iDNmZmb/y2P/yl3/yVv/ymD+zGf/zmb/yVn//vv/0Gb/+vD//Pb/hjD/68f/9uX/037/893/+Ov+3qL/gR7/7Mv+0HP+6L/+4q7/3Jn+5bb/14n/2ZL/8dlcYGb+36X/8ej/hCf/28b+jDn/1IH9t2LhuGaAd2buwWbGpWZvbGb/0XeKfWZZXmb+rFv/l07+x2r/0rf+pnD+vJX+kkP/7eD+z5f+pGifi2bQrGb/sYT/nmH/mFP+yauqk2b+r325nWb/vVv/2sP/fQz/u5L+qVhH49a3AAALJUlEQVR4nNWd6XbbNhCFIRkkuGhfqd0KbcVxIyd2tiZN4iZt0vd/o5KmJFK0SA3AGS73R4+TnhPyM4ABMBxcsFphanXHk/VssXUcgxmOs13MppNxt2UjP4Yh/3swtVbTpWNZmhCcc7YT50JoluYs5/0W4rMKIOzOF4YuQrKYONd0YzEZYD0ub8Lm3BFaElwEUxPbCU5/zZdwtbAAeHtIa9ZHeGaehJ2eBcULJPTFKvNT8yMcb8HNF2lIfdHN+Ny8CLs9XZ7vidGaZYus+RDaa0U+X4J3sjw7F8K+oynz+dKXGZoxD8KpwgCMNSNTj6r0hPZCz8jH/NE4V30+OeFgK7IDetJnii9ATdg1svbQvbSF2hqHmLCfeQiGEj0lRFrCfrYYGkfcqoRUUsJ+4gZCEbGn8BKUhAO0MXhAXMi/BSGhjQ7oRdSh9GsQEi5wpokY4kT2NegIp6hRJkSUXd2QEa5oABlnknMGFWGLYBAGko02VIQzikEYSJPbTBERriwyQE/N4glth6qP+hLL4gnnRGFmJ0smP0VC2CTl8+KpUzThmi7MBNIk5n0KwiZpmPHFDfgug4JwSN2EXiPCkxoEhC16QMYM8MqGgJA4kAaCj0R8Qtq5MFRxhGOE5GFcZsOXGf0rfVwY4RJ/GDaMn3evXn25iTLyTVGETfxR2Pjj8vLi4uLyzd1DBFEAP0qhE07QCRs/31wEunxthIhiXRDhBjvOmDd7QA/xrnH4e+7AJgxsQht9PdO4uwh1eRM2og5bf2MTokdS8/YiSvh32IgClnfDJkRfsZk3l1HESDdlRiGE6NN9dBjGCC1QNEUmHKCvZ8ybo176JUIopgUQEixozNdRwr+ikz7oMwYy4ZRgQfMzMhCjE6K3/IbMF8iES4JVd+PVAfHNTSP6fzTIfIFLaG8JCE3jLkC8vPjrCBC2D8YlpElBmeYfry7eXL7+cnsMyDgkrYhL2CXYOflqmLe3tw/H2ycf/eWnvAlXRIR+O5pxPmY+1N13ORPibyxSqe/b9dHnF7kS5pKiOQC+bdfrddf9kM6IS0gwHSYDvvQBfcb677ThiEtInuyOAD5c13dqu6P7x8SGxCXMIRccArbrodrur49fHz+9OHGWoaKE5sPHKGAA6Y7c6+/fPr/39eP3h6tPBIR59dITgDtMX26gkfvxEZ0wp0gT66KJao+uP1WS0HwJA/QZP9oVnA/Nt3UoYL0+eqzcmsZ8uIfzeY34DZewQ01oGvAeGugal7BP+/XXlGxAAsIBZRt6fP9JjMBA7g9cwpZRLj5vvnhRgSxGAKjC54XSd9iZqAUNoWm8lQwwO8Df6Lk2moVp4iLtHODXGn5GmCLUeDtBFcD26EMNn7BL0IaHra4s4GONgLBGEEwdpSHoulc1EkL8UKPWhKPv+00/NiH+ytRUiDJtP4gSEbbQ121hPgYs9/oqfCP0SoUecjc1H6Qb8NePaOVi+atN/pHspEcNSEHYxJ4v5Ajbo6+x0lP8qi/kcwhyvXT0/Sr+PviEuGcOGTPgsdTdTfLEhNhVUeD50HXjHZSKEPsTmwHa17dHn09/vaCo80Ze10Cyo+0TA5CQsIs863vBJh2xPbr/N/FtaM5bIAeb9BSw137JfESELezar5SO6o6+JfVPQkKCCsXTacS2++v9uWIFotN5Q+y1m2m8jW/02+7o+swXbkJC/BrFeLLGHdXfXxVQ9XVQFz9h4zXjLqZ6rTd6/+5885ES1iYECX7j4d5jG7n3H0Ctl0LYlTqGmiCKk8Bm48+rK0Ad1DnCsbZGIKTIf4OPkUR0gnCuc4FhdzfA76e6Qud6Tjj1XkwD1Ref0xgbUd4yonaCcBpM1hgjETvHD6t6jitOOA8AoUdu0oU8FDXocbUjxQgPHUuguGuifhPmW6V3OCYM34greN2cEGY/lXSL2OuIsBmx67CUukRcmB+FOYLX15FnjsSB8BTN0XYZQtGwLUp4/Daq/+Kx8FxcgEfV0gjjyQfwSdtUoZVJaYovECF8HtoxJkWsk1DKoS8kfP7LVpth40JKn8KOcaURnvpdo6zAkT7VKI+ZA+HJzY6eyf01ENKsb6kuQfaECTlOgeA3jbSuUZ289oQJeWqOEG1QUjbQk9uJhIldScYJJkEoSX6Vve8RYfJbiF5WRJR0hvo6OSBMK/TJjIiy+gYdxEshTH0JNWPUUCjl7RkJW+n/unAybRZRDmFk7KWdMxsAbmSZNFAWNXybKZaefQcuMkz9OPsnoTpt+YQQ9zFL3vx1J6SCTOXllU8I2uBoPcVrGJCyUZn2FrBCLc5VspV4VrSWYixgElVMKs2IVzSsupdjMgdduDWTHO+YX6AUE/FMblUl2FSC0R6i5vWFUqaGySamNTaEzv8dBzmrz1RWHqw2kH0NoW0656dfu+Mg3hqwQ1TZ6DCVugmui1knrbc2xzPIvVXyD1ZYIjPFhbHQxWbYef6x2B70O8ON9yvAhts9diO9eGPqljLcozSczXI9n0w6ncl8+nQRHtM0QWgNKaR3wixjksG/8E5oTxIi+bo4PElfj8BaZHYkRNIkvzYw7EJCekk47D4RUtiNEksuOczytZTBkSYz87M8DVewJLUIZ/nZkSBKZhHOiM61EkuD7+MYuqNqLpJIgbNt0S+rJnghBXOKflc1cZh5qU9I5xFAK7ifd1UJwTNGZQmZBsy9VZcQWu9TXUJo0VZVYymD+3lXlxDq54199jo/AW8Mqui69EmwEhs2qy4hrKS2mrunQLCByObVJYQVfjNyizVCCRAhnbkxvTTId7AKZhNDwTzZWxXupaAjJlmz+oUK6Dpf4SkfdHaJ5ehujC7QBorl7KOOKuDNAVRu/zkIVEXEas3q7oFBWVNGcMtWboIS5mgVjyxQvo0RuTnmImgbShfUlEbASEPhYZGTwIRV/Ej6JOCMT30ROqFA59me6ryr2oagRM0TIYVJRx6C39lV1W4K2+P7/7HRjiPnKw4AhJwKKq1g5fsBIbabYz6ClX/tTueRucVTCnY+YUdYyW2wCQHcE1Yx4yZ5W24FY43kjccEpk7Ugn7l3v9QuXUNsJOGhJWbMMDVJoefltVqRLCnUkhI4HRIKXjVV/hjpUaiROVe+CPksGxpBDeni7oooR33pJeETUaU0K5OSkri3POR11dlMqcyxfrHjnRVCTYyvl/HhK1qdFMpo7Uk38RSi8ucs4x7X84qMBQlzz3F/lyBeCrkjFyeObT2yx5sZI87P3fZ7ZR8KMq6R5xwSh6W+sO+JeuscMrtelPijio5CBMI7W1pERWshk56sjfLGlC5Ie8bcdpXf0B5pD6DLAUXoISbA7qsjIhK3sZJdyN0S/g5Ss0MOvH2h/IhWkouTin3WwyQ3WWyShEw7QaPVq9MiIp+5el3lNjL0izguFA2pk6/hWVaEkQOqklQIayNKZyCpCWYohsegLDW7BW/Dtc2WexFz98VNC26Ga1ZpjthALchrZwiMxtcl7OjUSGs1dZ6Yc0oMlmngglr3U1BjPoy861F0Du7OkYBXVXRblONsGZPqTzYEmVtMC67kbh3rbXmeTIKhtCAcoQe49TIa+rguqyRKAqhxzh3rDwYrZ76Mi0boafVwiJuSK4ZCLdOqBN6K7n5ltAb0ePLOMdnJ/TUX/doWpLrzgTlGqashL6N57xn6chNKawtwPxVTplu6bRXa4drGpLjJdf4DOFKlLgy30PaXM2XW25p2Zw9udDZsoPbPXdCuWnVHvQn6+WWWZYeuJjKwHIhdGs7HCNNf8+EeZes3equOvPpcLbobR3HMQAyDWcxnKyo6Hz9D7693wpudSblAAAAAElFTkSuQmCC" alt="">
                </div>
                <div class="col-md-5">
                    <ul>
                        <li>姓名: {{$users->user_kol_name}}</li>
                        <li></li>
                        <!-- <li>email: {{$users->email}}</li>
                        <li>登錄時間: {{$users->create_time}}</li>
                        <li>姓名: {{$users->name01}}</li>
                        <li>姓名: {{$users->name01}}</li> -->
                    </ul>
                </div>
                <div class="col-md-5">
                <ul>
                        <li>媒體一覽:</li>
                        <li>facebook: <a href="javascript:;">https://www.facebook.com</a></li>
                        <li>youtube: <a href="javascript:;">https://www.youtube.com</a></li>
                        <li>instagram: <a href="javascript:;">https://www.instagram.com</a></li>
                        <li>twitter: <a href="javascript:;">https://www.twitter.com</a></li>
                        <li>
                        <a href="javascript:;"><el-button type="info" round>合作申請</el-button></a>
                        <a href="javascript:;"><el-button type="info" round>連絡履歷</el-button></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="hello col-md-6">
                    <el-form ref="form" :model="form" label-width="80px">
                        <input_cokol type="textarea"></input_cokol>
                        <datepicker_cokol type="date" label="日期時間" name="datetime" picker="nobefore" placeholder="請選擇日期"></datepicker_cokol>
                        <daterangepicker_cokol type="datetimerange" label="日期時間" name="datetimerange" picker="nobefore"></daterangepicker_cokol>
                        <btn_cokol color="primary" rounded="false" content="送出" type="submit"></btn_cokol>
                        <btn_cokol color="danger" rounded="false" content="重設" type="reset"></btn_cokol>
                    </el-form>
                </div>
            </div>
        </div>
    </div>
    </section>
<!-- </div> -->
@endsection
