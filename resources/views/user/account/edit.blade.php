@extends('layouts.admin')

@section('content')
<!-- header -->
<section class="content-header">
	<h1>{{ __( 'user.account.title' ) }}</h1>
</section>


<!-- メインコンテンツ -->
<section class="content">


<div class="box box-primary">

    <form role="form" method="POST" action="{{ url('user/account/update') }}">
        @csrf
        <div class="box-body">
            
            <!--div class="form-group">
                <label for="icon">{{ __('user.account.form.icon') }}</label>
                <input type="file" id="icon">
                < <p class="help-block">Example block-level help text here.</p> >
            </div-->
            <div class="form-group">
                <label for="name01">{{ __('user.account.form.name01') }}</label>
                <input type="text" class="form-control" name="name01" placeholder="" value="{{ $user->name01 }}">
            </div>
            <div class="form-group">
                <label for="name02">{{ __('user.account.form.name02') }}</label>
                <input type="text" class="form-control" name="name02" placeholder="" value="{{ $user->name02 }}">
            </div>
            <div class="form-group">
                <label for="email">{{ __('user.account.form.email') }}</label>
                <input type="text" class="form-control" name="email" placeholder="" value="{{ $user->email }}">
            </div>
            <div class="form-group">
                <label for="tel_code">{{ __('user.account.form.tel_code') }}</label>
                <input type="text" class="form-control" name="tel_code" placeholder="" value="{{ $user->tel_code }}">
            </div>
            <div class="form-group">
                <label for="tel_num">{{ __('user.account.form.tel_num') }}</label>
                <input type="text" class="form-control" name="tel_num" placeholder="" value="{{ $user->tel_num }}">
            </div>
            <div class="form-group">
                <label for="zipcode">{{ __('user.account.form.zipcode') }}</label>
                <input type="text" class="form-control" name="zipcode" placeholder="" value="{{ $user->zipcode }}">
            </div>
            <div class="form-group">
                <label for="sex">{{ __('user.account.form.sex') }}</label>
                <input type="text" class="form-control" name="sex" placeholder="" value="{{ $user->sex }}">
            </div>
            <div class="form-group">
                <label for="fans">{{ __('user.account.form.fans') }}</label>
                <input type="text" class="form-control" name="fans" placeholder="" value="{{ $user->fans }}">
            </div>
            <div class="form-group">
                <label for="country">{{ __('user.account.form.country') }}</label>
                <input type="text" class="form-control" name="country" placeholder="" value="{{ $user->country }}">
            </div>
            <div class="form-group">
                <label for="lang">{{ __('user.account.form.lang') }}</label>
                <input type="text" class="form-control" name="lang" placeholder="" value="{{ $user->lang }}">
            </div>
            <div class="form-group">
                <label for="raward">{{ __('user.account.form.raward') }}</label>
                <input type="text" class="form-control" name="raward" placeholder="" value="{{ $user->raward }}">
            </div>
            <div class="form-group">
                <label for="wechat_account">{{ __('user.account.form.wechat_account') }}</label>
                <input type="text" class="form-control" name="wechat_account" placeholder="" value="{{ $user->wechat_account }}">
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>

</section>
@endsection