@extends('layouts.admin')

@section('content')
<!-- header -->
<section class="content-header">
	<h1>{{ __( 'title' ) }}</h1>
</section>


<!-- コンテンツヘッダ -->
<section class="content-header">
    <h1>Home</h1>
</section>

<!-- メインコンテンツ -->
<section class="content">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">XXX</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-hover">
                    <tbody>
                        <tr>
                            <th>id</th>
                            <th>name01</th>
                            <th>name02</th>
                            <th>email</th>
                            <th>phone</th>
                            <th>adddress</th>
                        </tr>
                        @foreach($users as $user)
                        <tr>
                                <td> {{$user->id}} </td>
                                <td> <a href="./{{$user->id}}">{{$user->name01}} </a></td>
                                <td> {{$user->name02}} </td>
                                <td> {{$user->email}} </td>
                                <td> {{$user->phone}} </td>
                                <td> {{$user->address}} </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
