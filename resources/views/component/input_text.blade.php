<div class="form-group">
    <label for="{{ $key ?? '' }}">{{ $label ?? '' }}</label>
    <input type="text" class="form-control" name="{{ $key ?? '' }}" placeholder="" value="{{ $value ?? '' }}">
</div>