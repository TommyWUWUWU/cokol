@extends('layouts.admin')

@section('content')
<!-- header -->
<section class="content-header">
	<h1>{{ __( 'system.header.title' ) }}</h1>
</section>

<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">{{ __( 'system.media.content.title' ) }}</h3>

			<div class="box-tools">
			<a href="{{ url('system/media/create') }}" class="btn btn-primary btn-sm ad-click-event"><i class="fas fa-plus"></i></a>
			</div>
		</div>

		<table id="example" class="table table-hover table-bordered" style="width:100%">
			<thead>
				<tr>
					<td>{{ __( 'system.media.table.id' ) }}</td>
					<td>{{ __( 'system.media.table.name' ) }}</td>
					<td></td>
				</tr>
			</thead>
			<tbody>	
				@foreach($medias as $media)
				<tr>
					<td>{{ $media->media_id }}</td>
					<td>{{ $media->media_name }}</td>
					<td>
						<a href="{{ url('system/media', $media->media_id) }}" class="btn btn-primary btn-sm ad-click-event"><i class="fas fa-pen"></i></a>
						
    					<form media="form" method="POST" action="{{ url('system/media', $media->media_id ?? '' ) }}">
							@method('DELETE')
							@csrf
							<button type="submit" class="btn btn-danger btn-sm ad-click-event"><i class="fas fa-times"></i></button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

	</div>
</section>
@endsection