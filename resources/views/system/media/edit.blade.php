@extends('layouts.admin')


@section('content')
<!-- header -->
<section class="content-header">
	<h1>{{ __( 'system.header.title' ) }}</h1>
</section>


<!-- メインコンテンツ -->
<section class="content">

<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title">{{ __( 'system.media.content.title' ) }}</h3>
	</div>

    <form media="form" method="POST" action="{{ url('system/media') }}">
        @csrf
        <div class="box-body">
            
			<input type="hidden" class="form-control" name="media_id" placeholder="" value="{{ $media->media_id ?? '' }}">
			
            @component('component.input_text', [
				'key' => 'media_name', 
				'value' => $media->media_name, 
				'label' => __('system.media.form.name') ])
			@endcomponent
			
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        <button type="submit" class="btn btn-primary"><i class="fas fa-check"></i> Submit</button>
        </div>
    </form>
</div>

</section>
@endsection