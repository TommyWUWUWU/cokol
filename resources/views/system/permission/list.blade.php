@extends('layouts.admin')

@section('content')
<!-- header -->
<section class="content-header">
	<h1>{{ __( 'system.header.title' ) }}</h1>
</section>

<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">{{ __( 'system.permission.content.title' ) }}</h3>

			<div class="box-tools">
			<a href="{{ url('system/permission/create') }}" class="btn btn-primary btn-sm ad-click-event"><i class="fas fa-plus"></i></a>
			</div>
		</div>

		<table id="example" class="table table-hover table-bordered" style="width:100%">
			<thead>
				<tr>
					<td>{{ __( 'system.permission.table.id' ) }}</td>
					<td>{{ __( 'system.permission.table.name' ) }}</td>
					<td>{{ __( 'system.permission.table.action' ) }}</td>
					<td></td>
				</tr>
			</thead>
			<tbody>	
				@foreach($permissions as $permission)
				<tr>
					<td>{{ $permission->id }}</td>
					<td>{{ $permission->name }}</td>
					<td>{{ $permission->action }}</td>
					<td>
						<a href="{{ url('system/permission', $permission->id) }}" class="btn btn-primary btn-sm ad-click-event"><i class="fas fa-pen"></i></a>

    					<form role="form" method="POST" action="{{ url('system/permission', $permission->id ?? '' ) }}">
							@method('DELETE')
							@csrf
							<button type="submit" class="btn btn-danger btn-sm ad-click-event"><i class="fas fa-times"></i></button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

	</div>
</section>
@endsection