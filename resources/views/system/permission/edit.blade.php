@extends('layouts.admin')


@section('content')
<!-- header -->
<section class="content-header">
	<h1>{{ __( 'system.header.title' ) }}</h1>
</section>


<!-- メインコンテンツ -->
<section class="content">


<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ __( 'system.permission.content.title' ) }}</h3>
    </div>

    <form role="form" method="POST" action="{{ url('system/permission') }}">
        @csrf
        <div class="box-body">
			<input type="hidden" class="form-control" name="id" placeholder="" value="{{ $permission->id ?? '' }}">
            
            @component('component.input_text', [
				'key' => 'name', 
				'value' => $permission->name ?? '', 
				'label' => __('system.permission.form.name') ])
			@endcomponent

            @component('component.input_text', [
				'key' => 'action', 
				'value' => $permission->action ?? '', 
				'label' => __('system.permission.form.action') ])
			@endcomponent
            
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        <button type="submit" class="btn btn-primary"><i class="fas fa-check"></i> Submit</button>
        </div>
    </form>
</div>

</section>
@endsection