@extends('layouts.admin')


@section('content')
<!-- header -->
<section class="content-header">
	<h1>{{ __( 'header.title' ) }}</h1>
</section>


<!-- メインコンテンツ -->
<section class="content">

<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title">{{ __( 'product.content.title' ) }}</h3>
	</div>

    <form product="form" method="POST" action="{{ url('product') }}">
        @csrf
        <div class="box-body">
            
			<input type="hidden" class="form-control" name="product_id" placeholder="" value="{{ $product->product_id ?? '' }}">
			
			@component('component.input_text', [
				'key' => 'product_name', 
				'value' => $product->product_name ?? '', 
				'label' => __('product.form.name') ])
			@endcomponent
			
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        <button type="submit" class="btn btn-primary"><i class="fas fa-check"></i> Submit</button>
        </div>
    </form>
</div>

</section>
@endsection