@extends('layouts.admin')

@section('content')
<!-- header -->
<section class="content-header">
	<h1>{{ __( 'product.header.title' ) }}</h1>
</section>

<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">{{ __( 'product.content.title' ) }}</h3>

			<div class="box-tools">
			<a href="{{ url('product/create') }}" class="btn btn-primary btn-sm ad-click-event"><i class="fas fa-plus"></i></a>
			</div>
		</div>

		<table id="example" class="table table-hover table-bordered" style="width:100%">
			<thead>
				<tr>
					<td>{{ __( 'product.table.id' ) }}</td>
					<td>{{ __( 'product.table.name' ) }}</td>
					<td></td>
				</tr>
			</thead>
			<tbody>	
				@foreach($products as $product)
				<tr>
					<td>{{ $product->product_id }}</td>
					<td>{{ $product->product_name }}</td>
					<td>
						<a href="{{ url('product', $product->product_id) }}" class="btn btn-primary btn-sm ad-click-event"><i class="fas fa-pen"></i></a>
						
    					<form product="form" method="POST" action="{{ url('product', $product->product_id ?? '' ) }}">
							@method('DELETE')
							@csrf
							<button type="submit" class="btn btn-danger btn-sm ad-click-event"><i class="fas fa-times"></i></button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

	</div>
</section>
@endsection