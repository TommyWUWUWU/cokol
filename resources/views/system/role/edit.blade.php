@extends('layouts.admin')


@section('content')
<!-- header -->
<section class="content-header">
	<h1>{{ __( 'system.header.title' ) }}</h1>
</section>


<!-- メインコンテンツ -->
<section class="content">

<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title">{{ __( 'system.role.content.title' ) }}</h3>
	</div>

    <form role="form" method="POST" action="{{ url('system/role') }}">
        @csrf
        <div class="box-body">

			<input type="hidden" class="form-control" name="role_id" placeholder="" value="{{ $role->role_id ?? '' }}">

			@component('component.input_text', [
				'key' => 'role_name',
				'value' => $role->role_name ?? '',
				'label' => __('system.role.form.name') ])
			@endcomponent

			<checkbox-component name="permission_ids[]" value="{{ $role ? $role->permissions->pluck('id') : '' }}"></checkbox-component>

        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        <button type="submit" class="btn btn-primary"><i class="fas fa-check"></i> Submit</button>
		</div>

    </form>
</div>

</section>
@endsection
