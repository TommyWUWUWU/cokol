@extends('layouts.admin')


@section('content')
<!-- header -->
<section class="content-header">
	<h1>{{ __( 'system.header.title' ) }}</h1>
</section>


<!-- メインコンテンツ -->
<section class="content">

<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title">{{ __( 'system.tag.content.title' ) }}</h3>
	</div>

    <form tag="form" method="POST" action="{{ url('system/tag') }}">
        @csrf
        <div class="box-body">
            
			<input type="hidden" class="form-control" name="tag_id" placeholder="" value="{{ $tag->tag_id ?? '' }}">
			
			@component('component.input_text', [
				'key' => 'tag_name', 
				'value' => $tag->tag_name ?? '', 
				'label' => __('system.tag.form.name') ])
			@endcomponent
			
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        <button type="submit" class="btn btn-primary"><i class="fas fa-check"></i> Submit</button>
        </div>
    </form>
</div>

</section>
@endsection