<?php

return [

	'title'              => [
        'register' => 'Register',
        'login'    => 'Login'
    ],
	'input_name01' => 'Last Name',
    'input_name02' => 'First Name',
    'input_email' => 'Email',
    'input_password' => 'Password',
    'input_confirm_password' => 'Confirm Password'

];
