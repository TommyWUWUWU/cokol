<?php

return [

	'title'              => [
        'register' => '新規登録',
        'login'    => 'ログイン'
    ],
	'input_name01' => '苗字',
    'input_name02' => '名前',
    'input_email' => 'メールアドレス',
    'input_password' => 'パスワード',
    'input_confirm_password' => 'パスワード確認'

];
