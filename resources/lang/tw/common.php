<?php

return [

	'title'              => [
        'register' => '註冊',
        'login'    => '登入',
        'user' => [
            'account' => '基本資料'
        ]
    ],
    'input_icon' => '上傳頭像',
    'input_name01' => '姓',
    'input_name02' => '名',
    'input_email' => '電子信箱',
    'input_password' => '密碼',
    'input_confirm_password' => '確認密碼',
    'input_tel_code' => '國際區碼',
    'input_tel_num' => '電話號碼',
    'input_zipcode' => '郵遞區號',
    'input_address' => '住址',
    'input_role_id' => '',
    'input_type_id' => '',
    'input_sex' => '性別',
    'input_fans' => '粉絲數',
    'input_create_time' => '建立時間',
    'input_client_id' => '',
    'input_country' => '國家',
    'input_lang' => '系統語言',
    'input_raward' => '期望報酬',
    'input_is_black' => '黑名單',
    'input_wechat_account' => 'weChat帳號'
];
