<?php

return [

	'account.title' => '基本資料',
	
    'account.form.icon' => '上傳頭像',
    'account.form.name01' => '姓',
    'account.form.name02' => '名',
    'account.form.email' => '電子信箱',
    'account.form.password' => '密碼',
    'account.form.confirm_password' => '確認密碼',
    'account.form.tel_code' => '國際區碼',
    'account.form.tel_num' => '電話號碼',
    'account.form.zipcode' => '郵遞區號',
    'account.form.address' => '住址',
    'account.form.role_id' => '',
    'account.form.type_id' => '',
    'account.form.sex' => '性別',
    'account.form.fans' => '粉絲數',
    'account.form.create_time' => '建立時間',
    'account.form.client_id' => '',
    'account.form.country' => '國家',
    'account.form.lang' => '系統語言',
    'account.form.raward' => '期望報酬',
    'account.form.is_black' => '黑名單',
    'account.form.wechat_account' => 'weChat帳號'
];
