<?php

return [

    'header.title'             => '系統設定',

    'role.content.title'       => '角色管理',
    'role.table.id'            => 'ID',
    'role.table.name'          => '角色',
    'role.form.name'           => '名稱',
    'role.form.permissions'    => '權限',

    'permission.content.title' => '權限管理',
    'permission.table.id'      => 'ID',
    'permission.table.name'    => '名稱',
    'permission.table.action'  => '權限',
    'permission.form.name'     => '名稱',
    'permission.form.action'   => '權限',

    'media.content.title'      => '媒體管理',
    'media.table.id'           => 'ID',
    'media.table.name'         => '名稱',
    'media.form.name'          => '名稱',

    'tag.content.title'        => '標籤管理',
    'tag.table.id'             => 'ID',
    'tag.table.name'           => '名稱',
    'tag.form.name'            => '名稱',
];
