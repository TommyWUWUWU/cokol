<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAdmin extends Model
{
    //
    protected $table = 'user_admin';
    public $primaryKey = 'user_admin_id';

    protected $fillable = ['user_admin_name'];

}
