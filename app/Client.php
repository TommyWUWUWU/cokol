<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $table = 'client';
    public $primaryKey = 'client_id';

    protected $fillable = ['client_name'];

}
