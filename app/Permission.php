<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    //
    protected $table = 'permission';
    public $primaryKey = 'id';
    
    protected $fillable = [
        'name', 'action',
    ];
    
    public function children() {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }  
}
