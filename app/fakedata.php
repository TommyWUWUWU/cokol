<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fakedata extends Model
{
    protected $table   = 'fakedata';
    public $primaryKey = 'id';

    protected $fillable = ['first_name', 'last_name', 'email', 'gender'];

}
