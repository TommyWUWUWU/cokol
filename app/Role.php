<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $table = 'role';
    public $primaryKey = 'role_id';

    protected $fillable = ['role_name'];
 
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permission', 'role_id', 'permission_id');
    }

    public function menus()
    {
        return $this->belongsToMany(Permission::class, 'role_permission', 'role_id', 'permission_id')->where('is_visible', true);
    }
}
