<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserKol extends Model
{
    //
    protected $table   = 'user_kol';
    public $primaryKey = 'user_id';

    protected $fillable = ['user_kol_name'];

    public function relation()
    {
        return $this->hasOne(User::class, 'id' . 'user_id');
    }
}
