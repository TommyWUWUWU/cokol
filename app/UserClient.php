<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserClient extends Model
{
    //
    protected $table = 'user_client';
    public $primaryKey = 'user_client_id';

    protected $fillable = ['user_client_name'];

}
