<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //
    protected $table = 'media';
    public $primaryKey = 'media_id';

    protected $fillable = ['media_name'];

}
