<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name01', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    //取得 user 可使用目錄  
    public function menus()
    {
        return $this->role->menus->where('parent_id', null);
    }

    public function role()
    {
        return $this->hasOne(Role::class, 'role_id', 'role_id');
    }

    public function hasPermissionWithAction($action)
    {
        // dd($action);
        if ( count($this->role->permissions->where('action', $action))>0 ) {
            return true;
        }
        return false;
    }
    public function kol()
    {
        return $this->hasOne(UserKol::class, 'user_id', 'id');
    }
}
