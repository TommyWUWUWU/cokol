<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $action = Route::current()->getActionName();
        // dd($action);
        $action = substr( $action, strrpos( $action, "\\")+1 );
        if ( ! $request->user()->hasPermissionWithAction($action) ) {
            return abort(401, 'Unauthorized action.');
        }
        return $next($request);
    }
}
