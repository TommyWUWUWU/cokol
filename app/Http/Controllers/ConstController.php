<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class ConstController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserType()
    {
        $list =  role::all();
        return view('system/role/list', compact('list'));
    }

    public function view($role_id = null)
    {
        $role =  role::find($role_id);
        return view('system/role/edit', compact('role'));
    }

    public function edit($role_id = null, Request $request)
    {
        $role = role::updateOrCreate(['role_id' => $role_id], ['role_name' => $request->get('role_name')]);
        return view('system/role/edit', compact('role'));
    }

    public function add(Request $request)
    {
        return view('system/role/edit');
    }
    
    public function delete($role_id)
    {
        role::destroy($role_id);
        return $this->list();
    }
    
}
