<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Http\Response
     */
    
    public function jsonList()
    {
        $clients =  client::simplePaginate(1);
        return response()->json($clients);
    }

    public function index()
    {
        $clients =  client::all();
        return view('client/list', compact('clients'));
    }

    public function show($id = null)
    {
        $client =  client::find($id);
        return view('client/edit', compact('client'));
    }

    public function store(Request $request)
    {
        $client = client::updateOrCreate(['client_id' => $request->get('client_id')], ['client_name' => $request->get('client_name')]);
        
        return view('client/edit', compact('client'));
    }

    public function create(Request $request)
    {
        return view('client/edit');
    }
    
    public function destroy($id)
    {
        DB::transaction(function() use ($id){
            client::destroy($id);
        });
        return $this->index();
    }
    
}
