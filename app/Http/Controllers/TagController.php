<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Http\Response
     */
    
    public function jsonList()
    {
        $tags =  tag::simplePaginate(1);
        return response()->json($tags);
    }

    public function index()
    {
        $tags =  tag::all();
        return view('system/tag/list', compact('tags'));
    }

    public function show($id = null)
    {
        $tag =  tag::find($id);
        return view('system/tag/edit', compact('tag'));
    }

    public function store(Request $request)
    {
        $tag = tag::updateOrCreate(['tag_id' => $request->get('tag_id')], ['tag_name' => $request->get('tag_name')]);
        
        return view('system/tag/edit', compact('tag'));
    }

    public function create(Request $request)
    {
        return view('system/tag/edit');
    }
    
    public function destroy($id)
    {
        DB::transaction(function() use ($id){
            tag::destroy($id);
        });
        return $this->index();
    }
    
}
