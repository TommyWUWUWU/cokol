<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Http\Response
     */
    
    public function jsonList()
    {
        $products =  product::simplePaginate(1);
        return response()->json($products);
    }

    public function index()
    {
        $products =  product::all();
        return view('system/product/list', compact('products'));
    }

    public function show($id = null)
    {
        $product =  product::find($id);
        return view('system/product/edit', compact('product'));
    }

    public function store(Request $request)
    {
        $product = product::updateOrCreate(['product_id' => $request->get('product_id')], ['product_name' => $request->get('product_name')]);
        
        return view('system/product/edit', compact('product'));
    }

    public function create(Request $request)
    {
        return view('system/product/edit');
    }
    
    public function destroy($id)
    {
        DB::transaction(function() use ($id){
            product::destroy($id);
        });
        return $this->index();
    }
    
}
