<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class MediaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Http\Response
     */
    
    public function jsonList()
    {
        $medias =  media::simplePaginate(1);
        return response()->json($medias);
    }

    public function index()
    {
        $medias =  media::all();
        return view('system/media/list', compact('medias'));
    }

    public function show($id = null)
    {
        $media =  media::find($id);
        return view('system/media/edit', compact('media'));
    }

    public function store(Request $request)
    {
        $media = media::updateOrCreate(['media_id' => $request->get('media_id')], ['media_name' => $request->get('media_name')]);
        
        return view('system/media/edit', compact('media'));
    }

    public function create(Request $request)
    {
        return view('system/media/edit');
    }
    
    public function destroy($id)
    {
        DB::transaction(function() use ($id){
            media::destroy($id);
        });
        return $this->index();
    }
    
}
