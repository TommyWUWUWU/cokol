<?php

namespace App\Http\Controllers;

use App\Fakedata;
use App\Permission;

class ChecktestController extends Controller
{
    public function index()
    {
        $permissions = permission::all('id', 'name');
        $fakedatas   = fakedata::all();
        return view('user/checktest')
            ->with('permissions', $permissions)
            ->with("fakedatas", $fakedatas);
    }
}
