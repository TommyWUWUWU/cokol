<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use DB;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Http\Response
     */

    public function jsonList()
    {
        $roles = role::simplePaginate(1);
        return response()->json($roles);
    }

    public function index()
    {
        $roles = role::all();
        return view('system/role/list', compact('roles'));
    }

    public function show($id = null)
    {
        $role = role::find($id);
        return view('system/role/edit', compact('role'));
    }

    public function store(Request $request)
    {
        $role = role::updateOrCreate(['role_id' => $request->get('role_id')], ['role_name' => $request->get('role_name')]);

        DB::table('role_permission')->where('role_id', $role->role_id)->delete();
        if ($request->get('permission_ids')) {
            $permissions = permission::whereIn('id', array_values($request->get('permission_ids')))->get();
            $role->permissions()->saveMany($permissions);
        }

        return view('system/role/edit', compact('role'));
    }

    public function create(Request $request)
    {
        return $this->show();
    }

    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            DB::table('role_permission')->where('role_id', $id)->delete();
            role::destroy($id);
        });
        return $this->index();
    }

}
