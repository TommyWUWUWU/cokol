<?php

namespace App\Http\Controllers;

use App\Permission;
use DB;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function jsonList()
    {
        $permissions = permission::all('id', 'name');
        return response()->json($permissions);
    }

    public function index()
    {
        $permissions = permission::all();
        return view('system/permission/list', compact('permissions'));
    }

    public function show($id = null)
    {
        $permission = permission::find($id);
        return view('system/permission/edit', compact('permission'));
    }

    public function store(Request $request)
    {
        $permission = permission::updateOrCreate(['id' => $request->get('id')],
            ['name'      => $request->get('name'), 'action' => $request->get('action'),
                'is_visible' => false]);

        return view('system/permission/edit', compact('permission'));
    }

    public function create(Request $request)
    {
        return view('system/permission/edit');
    }

    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            DB::table('role_permission')->where('permission_id', $id)->delete();
            permission::destroy($id);
        });
        return $this->index();
    }
}
