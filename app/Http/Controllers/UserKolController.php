<?php
namespace App\Http\Controllers;

use App\UserKol;
use Illuminate\Support\Facades\Auth; //
//

class UserKolController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function kolDetail($id)
    {
        $users = UserKol::find($id);
        $title = "請輸入啊啊啊啊啊";
        return view('user/detail')
            ->with('users', $users)
            ->with("title", $title);
    }

}
