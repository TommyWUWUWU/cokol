<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route; //
use Illuminate\Support\Facades\Auth;//

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function account()
    {
        $user =  Auth::user();
        return view('user/account/edit', compact('user'));
    }
    
    public function accountUpdate(Request $request)
    {
        $user= user::find(Auth::user()->id);
        // dd($user);
        $user->name01 = $request->get('name01');
        $user->name02 = $request->get('name02');
        $user->email = $request->get('email');
        $user->tel_code = $request->get('tel_code');
        $user->tel_num = $request->get('tel_num');
        $user->zipcode = $request->get('zipcode');
        $user->address = $request->get('address');
        $user->sex = $request->get('sex');
        $user->fans = $request->get('fans');
        $user->country = $request->get('country');
        $user->lang = $request->get('lang');
        $user->reward = $request->get('reward');
        $user->wechat_account = $request->get('wechat_account');

        // $user->role_id = $request->get('role_id');
        // $user->client_id = $request->get('client_id');

        $user->save();
        return redirect('user/account');
    }

    public function list()
    {
        $users =  user::all();
        return \View::make('user/account/list', compact('users'));
    }



    public function kolIndex()
    {
        $users =  user::all();
        return view('user/list', compact('users'));
    }
    
    public function kolDetail($id)
    {
        $users =  user::find($id);
        return view('user/detail')
            ->with('users', $users);
    }

}
