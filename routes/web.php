<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::middleware(['auth', 'auth.role'])->group(function () {
    // Home
    Route::get('/home', 'HomeController@index')->name('home');

    // User
    Route::get('/user/account', 'UserController@account');
    Route::post('/user/account/update', 'UserController@accountUpdate');

    // KOL
    Route::get('user/kol', 'UserController@kolIndex');
    Route::get('user/{id}', 'UserKolController@kolDetail');

    Route::get('user/kol/per', 'ChecktestController@index');

    // Client
    Route::resource('client', 'ClientController');

    // Product
    Route::resource('product', 'ProductController');

    Route::get('/user/walet', 'UserController@account');
    Route::get('share', 'UserController@account');
    Route::get('/share1', 'UserController@account');
    Route::get('/share', 'UserController@account');
    Route::get('/report', 'UserController@account');
    Route::get('/ranking', 'UserController@account');
    Route::get('/exam', 'UserController@account');

    // System
    Route::resource('/system/role', 'RoleController');
    Route::resource('/system/permission', 'PermissionController');
    Route::resource('/system/media', 'MediaController');
    Route::resource('/system/tag', 'TagController');

});

Route::middleware(['auth'])->group(function () {
    // API
    Route::get('/json/permission/list', 'PermissionController@jsonList');
});
